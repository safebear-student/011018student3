package com.safebear.auto.pages.Locators;

import lombok.Data;
import org.openqa.selenium.By;
@Data
public class ToolsPageLocators {
    private By SuccessfulLoginMessage = By.xpath(".//body/div[@class='container']/p/b");

    private By searchField = By.xpath(".//input[contains(@placeholder,\"Type the Name\")]");
    private By searchButton = By.xpath(".//button[@class=\"btn btn-info\"]");
    private By SeleniumTool = By.xpath(".//td[.=\"Selenium\"]");

}
