package com.safebear.auto.pages.Locators;

        import lombok.Data;
        import org.openqa.selenium.By;
@Data
public class LoginPageLocators {
    private By usernameLocator= By.id("username");
    private By passwordLocator = By.id("password");
    private By loginButtonLocator = By.id("enter");
    private By FailedLoginMessage=By.xpath(".//p[@id='rejectLogin']/b");
    private By SearchFieldLocator = By.id("toolName");
}
