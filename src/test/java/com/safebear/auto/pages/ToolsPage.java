package com.safebear.auto.pages;
import com.safebear.auto.pages.Locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {

    @NonNull
    WebDriver driver;

    ToolsPageLocators locators = new ToolsPageLocators();

    public String getPageTitle(){
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage()
    {
        return driver.findElement(locators.getSuccessfulLoginMessage()).getText();
    }

    //Entering text into the search field

    public void entertextintosearchfield(String toolname){
        driver.findElement(locators.getSearchField()).sendKeys(toolname);


    }

    //Entering text into the search field //clicking on the search button
    public void clickonsearchButton(){

        driver.findElement(locators.getSearchButton()).click();
    }
    //checking that the selenium tool has been returned/ checking the selenim tool exists

public boolean checktools(){

        return driver.findElement(locators.getSeleniumTool()).isDisplayed();
}

}
