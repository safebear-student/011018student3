Feature: Login
    In Order to access the website
    As a user
    I want to know if my login is successful

    Rules:
    * The User must be informed if the login information is incorrect
    * The User must be informed if the login is sucessful

    Glossary:
    * User: Someon who wants to create a Tools List using our application
    * Supporters: This is what the customer calls 'Admin' users.


    @HighRisk
    Scenario Outline: Navigate and login to the application
        Given I navigate to the login page
        When I enter the login details for a '<userType>'
        Then I can see the following message: '<validationMessage>'
        Examples:
        | userType    | validationMessage                 |
        | invalidUser | Username or Password is incorrect |
        | validUser   | Login Successful                  |

#    Scenario: Search for Selenuim once your loggedin
#        Given I enter the login details for a 'validUser'
#        When I search for Selenuim
#        Then Then Selenuim is returned